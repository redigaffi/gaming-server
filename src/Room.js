let BoardBuilderClass = require('./BoardBuilder.js');
let BoardManagerClass = require('./BoardManager.js');
const Api = require('./Api/Api.js');
const logger = require('./Service/Logger.js');
const FailedApiRequestModel = require('./Model/FailedApiRequestModel.js');

class Room {
    constructor (autoload = true, roomId, player1, player2) {
        this.turnTimeOutSeconds = process.env.PLAYER_TURN_TIMEOUT_SECONDS_INTERVAL;
        
        if (autoload) {    
            this.boardBuilder = new BoardBuilderClass();

            this.roomId = roomId;
            this.chatMessages = [];
            // Status, IN_PROGRESS, FINISHED
            this.status = "IN_PROGRESS";

            // Tag players with internal room id's.
            player1.setRoomPlayerId("p1");
            player2.setRoomPlayerId("p2");

            player1.setTurnTimeout(this.turnTimeOutSeconds);

            this.player1 = player1;
            this.player2 = player2;

            this.initBoard();

            this.initEventsForPlayer(this.player1);
            this.initEventsForPlayer(this.player2);
            this.sendMessageToBothClients("Match started, good luck!");
        }
    }

    static createFromCache(roomData, boardManager, player1, player2) {
        let room = new Room(false);
        room.roomId = roomData.roomId;
        room.status = "IN_PROGRESS";

        player1.room = room;
        player2.room = room;
        room.player1 = player1;
        room.player2 = player2;

        boardManager.room = room;
        room.boardManager = boardManager;

        if (room.boardManager.turn === "p1") {
            player1.setTurnTimeout(room.turnTimeOutSeconds);
        } else if (room.boardManager.turn === "p2"){
            player2.setTurnTimeout(room.turnTimeOutSeconds);
        }

        return room;
    }

    destroy () {
        this.boardBuilder = null;
        this.boardManager = null;
        this.player1 = null;
        this.player2 = null;
    }

    getRoomId() {
        return this.roomId;
    }

    gameFinished() {
        this.status = "FINISHED";
        this.player1.getSocket().disconnect();
        this.player2.getSocket().disconnect();
    }

    isGameFinished() {
        return this.status === "FINISHED";
    }

    isGameInProgress() {
        return this.status === "IN_PROGRESS";
    }

    initBoard() {
        let player1id = this.player1.getRoomPlayerId();
        let player2id = this.player2.getRoomPlayerId();

        this.boardBuilder.createCells(player1id, player2id);
        this.boardBuilder.placePieces(player1id, player2id);

        this.player1.addPieces(this.boardBuilder.getP1Pieces());
        this.player2.addPieces(this.boardBuilder.getP2Pieces());

        this.boardManager = new BoardManagerClass(this.boardBuilder.getBoard(), this);
    }

    initEventsForPlayer (player) {
        this.registerReadyEvent(player);
        this.registerMovePieceEvent(player);
        this.registerPlayerAbandonMatchEvent(player);
        //this.registerChatRoomEvent(player);
    }

    registerReadyEvent(player) {
        let opponent = this.getOpponentByPlayer(player.getRoomPlayerId());
        
        player.getSocket().emit('ready', {
            turnTimeout: player.getTurnoutTimeLeft(this.turnTimeOutSeconds),
            player: player.getRoomPlayerId(),
            myName: player.userData.username,
            strikes: player.getStrikes(),
            logoUrl: player.getLogoUrl(),
            eatenPiecesCount: player.getEatenPieceCount(), 
            opponentInfo: {
                name: opponent.userData.username,
                strikes: opponent.getStrikes(),
                logoUrl: opponent.getLogoUrl(),
                eatenPiecesCount: opponent.getEatenPieceCount()
            },
            chats: this.chatMessages,
            boardInfo: {
                playerTurn: this.boardManager.getTurn(),
                players: [this.player1.getRoomPlayerId(), this.player2.getRoomPlayerId()],
                board: this.boardManager.getBoard()
        }});
    }

    registerPlayerAbandonMatchEvent(player) {
        player.getSocket().on('abandon_match', () => this.playerAbandonedMatch(player));
    }

    registerMovePieceEvent (player) {
        player.getSocket().on('move_piece_event', this.movePieceHandler.bind(this));
    }

    registerChatRoomEvent (player) {
        player.getSocket().on('send_chat', this.incomingMessageHandler.bind(this));
    }

    sendMessageToBothClients (message) {
        /*let tmpData = {};
        tmpData.name = "Server";
        tmpData.message = message;

        this.chatMessages.push([
            tmpData.name,
            message
        ]);

        this.player1.getSocket().emit('received_message', tmpData);
        this.player2.getSocket().emit('received_message', tmpData);*/
    }

    incomingMessageHandler(data) {
        /*let roomPlayerId = data.roomPlayerId;
        let player = this.getPlayerByTag(roomPlayerId);
        let playerName = player.userData.username;

        this.chatMessages.push([
            playerName,
            data.message
        ]);

        let socketOpponent = this.getOpponentByPlayer(data.player);

        let tmpData = {};
        tmpData.name = playerName;
        tmpData.message = data.message;

        socketOpponent.getSocket().emit('received_message', tmpData);*/
    }

    turnLostDueTimeOut(player) {
        let opponent = this.getOpponentByPlayer(player.getRoomPlayerId());

        // User automatically lost due to hitting strike limit
        if (player.getStrikes() >= 3) {
            player.cancelTurnTimeout();
            opponent.cancelTurnTimeout();

            this.sendGameStatusOnUpdate(opponent, player);
            return;
        }

        this.boardManager.changeTurn();
        
        player.cancelTurnTimeout();
        opponent.setTurnTimeout(this.turnTimeOutSeconds);
        
        player.getSocket().emit('strike_added', {strikes: player.getStrikes()});

        opponent.getSocket().emit('change_turn', {
            reason: "turn_timed_out",
            opponentsStrikes: player.getStrikes(),
            turnTimeout: this.turnTimeOutSeconds,
        });
    }

    movePieceHandler(data) {
        let movePieceCorrect = this.boardManager.movePiece(data);
        let player = data.pieceToMove.player;
        let playerObject = this.getPlayerByTag(player);

        if (movePieceCorrect) {
            this.checkGameStatus(player);
            let opponentPlayer = this.getOpponentByPlayer(player);
            data.turnTimeout = this.turnTimeOutSeconds;
            data.opponentEatenPiecesCount = playerObject.getEatenPieceCount();
            opponentPlayer.getSocket().emit('piece_moved_event', data);

            if (data.changeTurn) {
                this.boardManager.changeTurn();
                opponentPlayer.setTurnTimeout(this.turnTimeOutSeconds);
                playerObject.cancelTurnTimeout();
            }
        } else {
            // Something weird happened, let's force him get a copy of the board stored in the server.
            logger.error(`User tried to move piece incorrectly, playerId: ${playerObject.getPlayerId()}`);
            playerObject.getSocket().emit('force_new_board');
        }
    }

    /**
     * Check if there is any winner or draw
     */
    checkGameStatus(currentPlayer) {
        // Current player has won the game (opponent has no pieces left)

        let currentPlayerObject = this.getPlayerByTag(currentPlayer);
        let opponentPlayer = this.getOpponentByPlayer(currentPlayer);
        let remainingOpponentPieces = opponentPlayer.countPiecesLeft();

        // Opponent has no pieces left (current player has won)
        if (remainingOpponentPieces === 0) {
            this.sendGameStatusOnUpdate(currentPlayerObject, opponentPlayer);
            return;

        } else if (remainingOpponentPieces > 0) { // Opponent has still pieces (check if he can move any piece)
            let canOpponentMoveAnyPiece = false;
            let opponentPieces = opponentPlayer.getPieces();

            for (let i = 0; i < opponentPieces.length; i++) {
                let currentOpponentPiece = opponentPieces[i];
                if (currentOpponentPiece !== undefined) {
                    let currentOpponentPieceMovements = currentOpponentPiece.getValidMovements(this.boardManager);
                    if (currentOpponentPieceMovements.countMovements() > 0) {
                        canOpponentMoveAnyPiece = true;
                        break;
                    }
                }
            }

            // All opponents pieces are blocked (currentplayer is the winner)
            if (!canOpponentMoveAnyPiece) {
                this.sendGameStatusOnUpdate(currentPlayerObject, opponentPlayer);
                return;
            }
        }

        if (this.boardManager.getPiecesMovedWithoutEating() >= 25) { // Tie
            this.sendGameStatusOnUpdate();
        }
    }

    /**
     * Send winner/looser status to each
     *
     * if no param is provided then it's tie.
     * @param winner 
     * @param looser
     */
    async sendGameStatusOnUpdate(winner, looser) {
        let dataToSend = {
            roomId: this.getRoomId(),
            winner: "",
            looser: "",
        };

        // Tie
        if (winner === undefined && looser === undefined) {
            try {
                await Api.updateMatchApiRequest(this.getRoomId(), dataToSend);
            } catch(exception) {
                logger.error(`Error when updating room tie status, roomId: ${this.getRoomId()}`);
                logger.info(dataToSend);
                logger.error(exception);

                new FailedApiRequestModel({
                    type: 'game_status_update',
                    attempts: 1,
                    data: dataToSend
                })
                .save()
                .catch(err => {
                    logger.error("Something failed storing failed api request (game tie update) for later attempt");
                    logger.error(err);
                });
            }
            
            this.player1.getSocket().emit("game_status", {status: "tie"});
            this.player2.getSocket().emit("game_status", {status: "tie"});
            this.gameFinished();
            return;
        }

        dataToSend.winner = winner.userData.id;
        dataToSend.looser = looser.userData.id;

        try {
            await Api.updateMatchApiRequest(this.getRoomId(), dataToSend);
        } catch(exception) {
            logger.error(`Error when updating room winning status, roomId: ${this.getRoomId()}`);
            logger.info(dataToSend);
            logger.error(exception);

            new FailedApiRequestModel({
                type: 'game_status_update',
                attempts: 1,
                data: dataToSend
            })
            .save()
            .catch(err => {
                logger.error("Something failed storing failed api request (game win update) for later attempt");
                logger.error(err);
            });
        }
        
        winner.getSocket().emit("game_status", {status: "win"});
        looser.getSocket().emit("game_status", {status: "lost"});
        this.gameFinished();
    }

    /**
     * @param player - User who left/abandoned the room
     */
    playerAbandonedMatch(player) {
        let winner = this.getOpponentByPlayer(player.getRoomPlayerId());
        let looser = this.getPlayerByTag(player.getRoomPlayerId());
        this.sendGameStatusOnUpdate(winner, looser);
    }

    getOpponentByPlayer(player) {
        if (player === this.player1.getRoomPlayerId()) {
            return this.player2;
        } else if (player === this.player2.getRoomPlayerId()) {
            return this.player1;
        }
    };

    getPlayerByTag(tag) {
        if (tag === this.player1.getRoomPlayerId()) {
            return this.player1;
        } else if (tag === this.player2.getRoomPlayerId()) {
            return this.player2;
        }
    }
};

module.exports = Room;
