const crypto = require('crypto');

class Player {

    constructor (playerId, socket, userData) {
        this.playerId = playerId;
        this.socket = socket;
        this.room = false;
        this.roomPlayerId = "";
        this.pieces = new Map();
        this.eatenPieces = new Map();
        this.socketIds = [];
        if (socket !== undefined) {
            this.socketIds.push(socket.id);
        }
        this.disconnectTimeout;
        this.turnTimeoutInterval;
        this.userData = userData;
        this.strikes = 0;
    }

    static createFromCache(playerData, myPieces, eatenPieces) {
        
        let player = new Player();
        player.playerId = playerData.playerId;
        player.roomPlayerId = playerData.roomPlayerId
        playerData.socketIds.forEach(socketId => player.socketIds.push(socketId));
        player.addPieces(myPieces);
        player.addEatenPieces(eatenPieces);
        player.strikes = playerData.strikes
        player.userData = playerData.userData;
        return player;
    }

    destroy () {
        this.room = false;
        this.socket = null;
        
        this.pieces.forEach((piece, key, map) => {
            piece.player = null;
            this.removePiece(piece)
        });

        this.eatenPieces.forEach((piece) => {
            piece.player = null;
        });
        
        this.pieces = null;
        this.eatenPieces = null;
        
        clearTimeout(this.turnTimeoutInterval);
        clearTimeout(this.disconnectTimeout);

    }

    disconnectSetTimeoutToAbandonIfInMatch() {
        if (this.hasRoom() && !this.getRoom().isGameFinished()) {    
            this.room.sendMessageToBothClients(this.userData.username + " closed the windows. Countdown to abandon the match started.");
            this.disconnectTimeout = setTimeout(this.abandonMatch.bind(this), process.env.DISONNECT_TIMEOUT_TO_ABANDON_MATCH); // 5 min
            
        }
    }

    getLogoUrl() {
        let email = this.userData.email;
        if (email !== undefined) {
            let emailHash = crypto.createHash('md5').update(email).digest("hex");
            return `https://www.gravatar.com/avatar/${emailHash}`;
        }
        
        // Default image
        return "https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?f=y";
    }

    /**
     * @param seconds - Seconds to timeout
     */
    setTurnTimeout(seconds) {
        console.log("setting timeout");
        this.turnTimeoutInterval = setTimeout(this.turnTimedOut.bind(this), (seconds*1000));
    }

    /**
     * @param fallbackTime - In case no timeout available, use fallback
     */
    getTurnoutTimeLeft(fallbackTime) {
        if (this.turnTimeoutInterval !== undefined) {
            return Math.ceil((this.turnTimeoutInterval._idleStart + this.turnTimeoutInterval._idleTimeout)/1000 - process.uptime());
        }
        
        return fallbackTime;
    }

    cancelTurnTimeout() {
        clearTimeout(this.turnTimeoutInterval);
    }

    getStrikes() {
        return this.strikes;
    }

    turnTimedOut() {
        this.strikes += 1;
        this.room.turnLostDueTimeOut(this);
    }

    abandonMatch() {
        this.room.playerAbandonedMatch(this);
    }

    addPieces (pieces) {
        pieces.forEach(piece => {
            this.pieces.set(piece.getId(), piece);
        })
    }

    getPieces() {
        let pieces = [];
        this.pieces.forEach((value, key, map) => {
            pieces[key] = value;
        });
        
        return pieces;
    }

    getEatenPieces() {
        let eatenPieces = [];
        this.eatenPieces.forEach((value, key, map) => {
            eatenPieces[key] = value;
        });
        
        return eatenPieces;
    }

    addEatenPiece (eatenPiece) {
        this.eatenPieces.set(eatenPiece.getId(), eatenPiece);
    }

    addEatenPieces (eatenPiecesCache) {
        eatenPiecesCache.forEach(eatenPieceCache => {
            this.eatenPieces.set(eatenPieceCache.getId(), eatenPieceCache);
        })
    }

    getEatenPieceCount () {
        return this.eatenPieces.size;
    }

    removePiece (piece) {
        this.pieces.delete(piece.getId());
    }

    countPiecesLeft() {
        return this.pieces.size;
    }

    setRoom (room) {
        this.room = room;
    }

    getRoom() {
        return this.room;
    }

    hasRoom () {
        return this.room !== false;
    }

    getSocketIds() {
        return this.socketIds;
    }
    
    updateSocket(socket) {
        this.socket = socket;
        this.socketIds.push(socket.id);

        // Remove disconnect timeout (User reconnected)
        if (this.disconnectTimeout !== undefined) {
            this.room.sendMessageToBothClients(this.userData.username + " Joined again, removing countdown to abandon.");
            clearTimeout(this.disconnectTimeout);
        }

        // If player was in a room init all socket events again
        if (this.hasRoom()) {
            this.room.initEventsForPlayer(this);
        }
    }

    getUserData() {
        return this.userData;
    }
    
    getSocket() {
        return this.socket;
    }

    getPlayerId() {
        return this.playerId;
    }

    getRoomPlayerId() {
        return this.roomPlayerId;
    }

    setRoomPlayerId(roomPlayerId) {
        this.roomPlayerId = roomPlayerId;
    }



};

module.exports = Player;
