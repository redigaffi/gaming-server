/**
 * @type ./Dto/Cell
 */
let CellClass = require('./Dto/Cell.js');
let PieceClass = require('./Dto/Piece.js');

class BoardBuilder {
    constructor () {
        this.p1Pieces = [];
        this.p2Pieces = [];

        this.yOffset = 10;
        this.xOffset = 10;
        this.piecesPerPlayer = 20;
        this.board = [
            ["", "", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", "", ""]
        ];
    };

    static createFromCache(roomData) {
        let boardBuilder = new BoardBuilder()
        boardBuilder.createCells("p1", "p2");
        boardBuilder.placePiecesFromCache(roomData.board);
        return boardBuilder;
    }

    placePiecesFromCache(boardData) {
    
        for (let y = 0; y < this.yOffset; y++) {
            for (let x = 0; x < this.xOffset; x++) {

                let cell = this.board[y][x];
                
                let cellCache = boardData[y][x];
                if (cellCache.piece !== false) {
                    let newPieceFromCache = new PieceClass(cellCache.piece.player, cellCache.piece.id, cellCache.piece.y, cellCache.piece.x);
                    if (cellCache.piece.isKing) {
                        newPieceFromCache.convertToKing();
                    }

                    if (newPieceFromCache.player === "p1") {
                        this.p1Pieces.push(newPieceFromCache);
                    } else {
                        this.p2Pieces.push(newPieceFromCache);
                    }

                    cell.setPiece(newPieceFromCache);
                }
            }
        }
    }

    createCells(player1, player2) {
        let pair = false;

        for (let y = 0; y < this.yOffset; y++) {
            for (let x = 0; x < this.xOffset; x++) {
                let cell = new CellClass(y, x);

                if (y === 0) {
                    cell.setPlayerFirstRow(player1);
                }

                if (y === this.yOffset-1) {
                    cell.setPlayerFirstRow(player2);
                }

                if ( (!pair && x%2 > 0) || (pair && x%2 === 0) ) {
                    cell.setIsMovable(true);
                }

                this.board[y][x] = cell;
            }

            pair = !pair;
        }

    };

    placePieces(player1, player2) {
        let remainingPieces = this.piecesPerPlayer;

        for (let y = 0; y < this.yOffset; y++) {
            for (let x = 0; x < this.xOffset; x++) {
                let cell = this.board[y][x];

                if (cell.isMovable() && remainingPieces > 0) {
                    let piece = new PieceClass(player1, this.piecesPerPlayer-remainingPieces, y, x);
                    this.p1Pieces.push(piece);
                    cell.setPiece(piece);
                    remainingPieces--;
                }
            }
        }

        remainingPieces = this.piecesPerPlayer;
        for (let y = this.yOffset-1; y >= 0; y--) {
            for (let x = this.xOffset-1; x >= 0; x--) {
                let cell = this.board[y][x];

                if (cell.isMovable() && remainingPieces > 0) {
                    let piece = new PieceClass(player2, ((this.piecesPerPlayer-remainingPieces)+20), y, x);
                    this.p2Pieces.push(piece);
                    cell.setPiece(piece);
                    remainingPieces--;
                }
            }
        }
    };

    getP1Pieces() {
        return this.p1Pieces;
    }

    getP2Pieces() {
        return this.p2Pieces;
    }

    getBoard() {
        return this.board;
    }
};

module.exports = BoardBuilder;
