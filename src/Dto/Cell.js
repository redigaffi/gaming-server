class Cell {
    constructor(y, x) {
        this.movable = false;
        this.piece = false;
        this.y = y;
        this.x = x;
        this.playerFirstRow = "";
    }

    setIsMovable(isMovable) {
        this.movable = isMovable;
    }

    isMovable() {
        return this.movable;
    }

    setPiece(piece) {
        this.piece = piece;
    }

    getPiece() {
        return this.piece;
    }

    hasPiece() {
        return this.piece !== false;
    }

    removePiece() {
        this.piece = false;
    }

    getPlayerFirstRow() {
        return this.playerFirstRow;
    }

    setPlayerFirstRow(playerFirstRow) {
        this.playerFirstRow = playerFirstRow;
    }

    getCellY() {
        return this.y;

    }

    getCellX() {
        return this.x;
    }
};

module.exports = Cell;

