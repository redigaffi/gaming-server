function ValidMovement(pieceId, operationType, y, x, canEatPiece = false, cellToEat = false) {
    this.pieceId = pieceId;
    this.operationType = operationType;
    this.y = y;
    this.x = x;
    this.canEatPiece = canEatPiece;
    this.cellToEat = cellToEat;
}

ValidMovement.prototype.getY = function() {
    return this.y;
};

ValidMovement.prototype.getX = function() {
    return this.x;
};

ValidMovement.prototype.getOperationType = function() {
    return this.operationType;
};

ValidMovement.prototype.getCanEatPiece = function() {
    return this.canEatPiece;
};

ValidMovement.prototype.getCellToEat = function() {
    return this.cellToEat;
};

ValidMovement.prototype.setCellToEat = function(cellToEat) {
    this.cellToEat = cellToEat;
};

ValidMovement.prototype.setCanEatPiece = function(canEatPiece) {
    this.canEatPiece = canEatPiece;
};

module.exports = ValidMovement;