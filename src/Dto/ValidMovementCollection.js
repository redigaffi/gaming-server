function ValidMovementCollection(validMovements = []) {
    this.validMovements = [];
    this.canAnyMovementEat = false;

    if (validMovements.length > 0) {
        for(let i = 0; i < validMovements.length; i++) {
            let validMovement = validMovements[i];
            if (validMovement.getCanEatPiece()) {
                this.canAnyMovementEat = true;
            }

            this.validMovements.push(validMovement);
        }
    }

}
ValidMovementCollection.prototype.countMovements = function() {
    return this.validMovements.length;
};

ValidMovementCollection.prototype.push = function(validMovement) {
    if (validMovement.getCanEatPiece()) {
        this.canAnyMovementEat = true;
    }

    this.validMovements.push(validMovement);
};

ValidMovementCollection.prototype.getValidMovements = function() {
    return this.validMovements;
};

ValidMovementCollection.prototype.getCanAnyMovementEat = function() {
    return this.canAnyMovementEat;
};

ValidMovementCollection.prototype.asArray = function() {
    return this.validMovements;
};

ValidMovementCollection.prototype.getByOperationType = function (operationType) {
    let movementByOperation = [];

    for(let i = 0; i < this.validMovements.length; i++) {
        let validMovement = this.validMovements[i];
        if (validMovement.getOperationType() === operationType) {
            movementByOperation.push(validMovement);
        }
    }

    return movementByOperation;
};

module.exports = ValidMovementCollection;