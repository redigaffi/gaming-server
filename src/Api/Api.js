const request = require('request');

class Api {

    async _makeRequest(options) {
        return new Promise((resolve, reject) => {
                request(options, (error, response, body) => {
                    if (!error && response.statusCode == 200) {
                        resolve(body);
                    } else {
                        reject(error);
                    }})
            });
    }

    async getUserByAuthenticationToken(auth) {
        const options = {
            url: `${process.env.API_BASE_URL}/api/user`,
            method: "GET",
            headers: {
                'Authorization': auth.accessToken
            },
            body: {},
            json: true,
        };

        return this._makeRequest(options);
        
    }

    async createMatchApiRequest(player1Id, player2Id) {
        const data = {
            player1: player1Id,
            player2: player2Id,
        };

        const options = {
            url: `${process.env.API_BASE_URL}/api/newMatch`,
            method: "POST",
            json: true,
            body: data
        };
        
        return this._makeRequest(options);
    }

    async updateMatchApiRequest(roomId, data) {
        const options = {
            url: `${process.env.API_BASE_URL}/api/matchTerminated/${roomId}`,
            method: "POST",
            json: true,
            body: data
        };

        return this._makeRequest(options);
    }

    async canUserPlay(auth) {
        const options = {
            url: `${process.env.API_BASE_URL}/api/canPlay`,
            method: "POST",
            headers: {
                'Authorization': auth.accessToken
            },
            json: true,
        };

        return this._makeRequest(options);
    }
}

 // Singleton
 module.exports = new Api();