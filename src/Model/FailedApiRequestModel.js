let mongoose = require('mongoose');

let failedApiRequestSchema = new mongoose.Schema({
  type: String,
  processing: Boolean,
  attempts: Number,
  data: Object
});

module.exports = mongoose.model('FailedApiRequest', failedApiRequestSchema);