let mongoose = require('mongoose');

let roomSchema = new mongoose.Schema({
  roomId: Number,
  player1Id: Number,
  player2Id: Number,
  currentTurn: String,
  piecesMovedWithoutEating: Number,
  board: Object
});

module.exports = mongoose.model('Room', roomSchema)

/**
 * 
 * Add status that the issue maybe that board is not marked as finished and deleted.
 */