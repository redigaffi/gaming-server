let mongoose = require('mongoose');

let playerSchema = new mongoose.Schema({
  playerId: Number,
  roomId: Number,
  roomPlayerId: String,
  socketIds: Array,
  pieces: Array,
  eatenPieces: Array,
  userData: Object,
  strikes: Number  
});

module.exports = mongoose.model('Player', playerSchema)