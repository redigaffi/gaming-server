const PlayerPool = require('./PlayerPool.js');
const BoardBuilder = require('./BoardBuilder.js');
const RoomClass = require('./Room.js');

const RoomModel = require('./Model/RoomModel.js');
const PlayerModel = require('./Model/PlayerModel.js');
const FailedApiRequestModel = require('./Model/FailedApiRequestModel.js');

const BoardManager = require('./BoardManager.js');
const PlayerClass = require('./Player.js');
const PieceClass = require('./Dto/Piece.js');
const Api = require('./Api/Api.js');

const logger = require('./Service/Logger.js');

/**
 * 1. Socket event handlers
 * 2. Store rooms/players by socket/players
 * 3. Match players
 * 4. Clear finished matches
 * 5. Store Room States
 */
 class Server {
    constructor() {
        this.players             = new Map();
        this.playersBySocketId   = new Map();
        this.rooms               = new Map();
    }

    async recoverStateFromCache() {
        RoomModel
        .find({})
        .then(roomDocs => {
            logger.debug(`There are ${roomDocs.length} rooms to recover`);
            roomDocs.forEach(async currentRoom => {
                
                try {
                    const boardBuilder = BoardBuilder.createFromCache(currentRoom);
                    const boardManager = BoardManager.createFromCache(currentRoom, boardBuilder.getBoard());;
                    
                    let player1 = await this._rebuildPlayerFromCache(currentRoom.player1Id, boardBuilder.getP1Pieces());
                    let player2 = await this._rebuildPlayerFromCache(currentRoom.player2Id, boardBuilder.getP2Pieces());

                    let room = RoomClass.createFromCache(currentRoom, boardManager, player1, player2);
                    this._storeStateInMemory(player1, player2, room);

                } catch(exception) {   
                    logger.error("Error recovering state on server bootup");
                    logger.error(exception);

                }
                
            });
        }).catch(error => {
            logger.error("Something failed when connecting to mongoDB to recover state");
            logger.error(error)
        });

    }

    async _rebuildPlayerFromCache(playerId, playerPieces) {
        return PlayerModel
            .find({playerId: playerId})
            .then(playerData =>  {

                let player = playerData[0];
                let eatenPieces = [];
                
                player.eatenPieces.forEach(eatenPieceCache => {
                    if (eatenPieceCache !== null) {
                        eatenPieces.push(PieceClass.createFromCache(eatenPieceCache));
                    }
                });
                
                return PlayerClass.createFromCache(playerData[0], playerPieces, eatenPieces);        
            })
            .catch(error => {
                logger.error("Something failed rebuilding user instance from cache");
                logger.error(error);
            });
    }

    async _storeStateInMemory(player1, player2, room) {
        this.players.set(player1.getPlayerId(), player1);
        this.players.set(player2.getPlayerId(), player2);
        
        player1.getSocketIds().forEach(socketId => this.playersBySocketId.set(socketId, player1));
        player2.getSocketIds().forEach(socketId => this.playersBySocketId.set(socketId, player2));

        this.rooms.set(room.roomId, room);
    }

    /**
     * @todo: Finish scenario if user cant play (e.g insufficient balance)
     */
    async startSessionEvent(data, socket) {
        try {
            logger.debug("Start session event called");
           
            const canUserPlay = await Api.canUserPlay(data);
                
            if (canUserPlay.canPlay === false) {
                // dont continue execution
                // send user feedback so he knows why he cant play
            }
            
            const user = await Api.getUserByAuthenticationToken(data);            
            const userId = user.id;
            
            // New user
            if (!this.players.has(userId)) {
                let newPlayer = new PlayerClass(userId, socket, user);
                this.players.set(userId, newPlayer);
                this.playersBySocketId.set(socket.id, newPlayer);
                PlayerPool.addPlayer(newPlayer);
                logger.debug("New user added to the pool");
            // Update existing user (Page refresh, new token, etc)
            } else {
                let player = this.players.get(userId);
                player.updateSocket(socket);
                this.playersBySocketId.set(socket.id, player);
                logger.debug("Already existing user reconnected - user updated");
            }

        } catch(exception) {
            logger.error("Something failed in startSessionEvent");
            logger.error(exception);
        }
        
    }

    async disconnectEvent(socket) {
        
        if (!this.playersBySocketId.has(socket.id)) { 
            return;
        }

        let disconnectedUser = this.playersBySocketId.get(socket.id);
    
        // @todo: if we wasn't in a room/playing remove him directly
        
        if (!disconnectedUser.hasRoom()) {
            PlayerPool.removeFromQueue(disconnectedUser);
            disconnectedUser.getSocketIds().forEach(socketId => this.playersBySocketId.delete(socketId));
            PlayerModel.findOneAndRemove({playerId: disconnectedUser.getPlayerId()});
            disconnectedUser.destroy();
            this.players.delete(disconnectedUser.getPlayerId());
            return;
        }

        disconnectedUser.disconnectSetTimeoutToAbandonIfInMatch();
    }

    async matchPlayersAndCreateRoom() {
        try {
            let [player1, player2] = await PlayerPool.matchPlayers();
            
            if (!player1 || !player2) {
                return;
            }

            logger.info("Matched players, creating room.");

            // Notify clients room is being prepared
            player1.getSocket().emit('opponent_found');
            player2.getSocket().emit('opponent_found');

            const roomData = await Api.createMatchApiRequest(player1.userData.id, player2.userData.id);
            const room = new RoomClass(true, roomData.roomId, player1, player2);
            

            player1.setRoom(room);
            player2.setRoom(room);
            
            this.rooms.set(roomData.roomId, room);

        } catch (exception) {
            logger.error("Something failed in matchPlayersAndCreateRoom event");
            logger.error(exception);
        }

    }

    async storeState() {
        this.rooms.forEach(async (room, key, map) => {
            if (room.isGameInProgress()) {
                RoomModel
                    .find({roomId: room.getRoomId()})
                    .then(async roomDoc => {
                        
                        if(roomDoc.length <= 0) { 
                            await this._createRoomAndPlayersInCache(room);
                        } else {
                            await this._updateRoomAndPlayersInCache(roomDoc[0], room);
                        }

                    }) 
                    .catch(err => {
                        logger.error("Something went wrong storing state");
                        logger.error(err);
                    }); 
            }
        });
    }

    async _updateRoomAndPlayersInCache(roomDoc, room) {
         // Current turn
        roomDoc.board = room.boardManager.board;
        roomDoc.currentTurn = room.boardManager.turn;
        roomDoc.piecesMovedWithoutEating = room.boardManager.getPiecesMovedWithoutEating();
        roomDoc.save();

        PlayerModel
            .find({playerId: roomDoc.player1Id})
            .then(playerDoc => {

                playerDoc[0].pieces = room.player1.getPieces();
                playerDoc[0].eatenPieces = room.player1.getEatenPieces();
                playerDoc[0].socketIds = room.player1.socketIds;
                playerDoc[0].strikes = room.player1.strikes;
                playerDoc[0].save(); 
            }).catch(err => {
                logger.error("something wrong saving user1 mongodb");
                logger.error(err);
            });
        
        PlayerModel
            .find({playerId: roomDoc.player2Id})
            .then(playerDoc => {
                playerDoc[0].pieces = room.player2.getPieces();
                playerDoc[0].eatenPieces = room.player2.getEatenPieces();
                playerDoc[0].socketIds = room.player2.socketIds;
                playerDoc[0].strikes = room.player2.strikes;
                playerDoc[0].save(); 
            }).catch(err => {
                logger.error("something wrong saving user2 mongodb");
                logger.error(err);
            });
    }

    async _createRoomAndPlayersInCache(room) {
        new RoomModel({
            roomId: room.getRoomId(),
            player1Id: room.player1.getPlayerId(),
            player2Id: room.player2.getPlayerId(),
            currentTurn: room.boardManager.turn,
            board: room.boardManager.board
        })
        .save()
        .catch(err => {
            logger.error("something wrong creating a room in mongodb");
            logger.error(err);
        });
    

        new PlayerModel({
            playerId: room.player1.getPlayerId(),
            roomId: room.getRoomId(),
            roomPlayerId: room.player1.roomPlayerId,
            socketIds: room.player1.getSocketIds(),
            pieces: room.player1.getPieces(),
            eatenPieces: room.player1.getEatenPieces(),
            userData: room.player1.getUserData(), 
            strikes: 0,
        })
        .save()
        .catch(err => {
            logger.error("something wrong creating a player in mongodb");
            logger.error(err);
        });
            
        
        new PlayerModel({
            playerId: room.player2.getPlayerId(),
            roomId: room.getRoomId(),
            roomPlayerId: room.player2.roomPlayerId,
            socketIds: room.player2.getSocketIds(),
            pieces: room.player2.getPieces(),
            eatenPieces: room.player2.getEatenPieces(),
            userData: room.player2.getUserData(), 
            strikes: 0,
        })
        .save()
        .catch(err => {
            logger.error("something wrong creating a player in mongodb");
            logger.error(err);
        });
    }

    async clearFinishedMatches() {
        this.rooms.forEach(async (room, key, map) => {
            if (room.isGameFinished()) { 
                RoomModel
                    .findOneAndRemove({roomId: room.getRoomId()})
                    .then()
                    .catch(err => {
                        logger.error("something wrong deleting a room in mongodb");
                        logger.error(err);
                    });

                let player1 = room.player1;
                let player2 = room.player2;

                this.players.delete(player1.getPlayerId());
                this.players.delete(player2.getPlayerId());

                PlayerModel
                    .findOneAndRemove({playerId: player1.getPlayerId()})
                    .then()
                    .catch(err => {
                        logger.error("something wrong deleting a player in mongodb");
                        logger.error(err);
                    });

                PlayerModel
                    .findOneAndRemove({playerId: player2.getPlayerId()})
                    .then()
                    .catch(err => {
                        logger.error("something wrong deleting a player in mongodb");
                        logger.error(err);
                    });

                
                player1.getSocketIds().forEach((socketId) => {
                    this.playersBySocketId.delete(socketId);
                });

                player2.getSocketIds().forEach((socketId) => {
                    this.playersBySocketId.delete(socketId);
                });

                player1.destroy();
                player2.destroy();

                this.rooms.delete(room.roomId);
                room.boardManager.destroy();
                room.destroy();
            }
        });
    }

    async retryFailedApiRequests() {
        
        FailedApiRequestModel
        .find({})
        .then(failedRequests => {
            failedRequests.forEach(async failedRequest => {
                if (failedRequest.processing) {
                    return;
                }

                if(failedRequest.type === "game_status_update") {
                    try {
                        failedRequest.processing = true;
                        await failedRequest.save();
                        const data = failedRequest.data;
                        await Api.updateMatchApiRequest(data.roomId, data);
                        await failedRequest.remove();
                    } catch(exception) {
                        logger.error(`Error when re-attempting game_status_update API call, attempts: ${failedRequest.attempts}`);
                        logger.error(exception);
                        failedRequest.processing = false;
                        failedRequest.attempts += 1; 
                        await failedRequest.save();
                    }   
                }

                if (failedRequest.attempts >= 10) {
                    logger.error(`Stopped retrying ${failedRequest.type} after 10 retries API call, attempts: ${failedRequest.attempts}`);
                    await failedRequest.delete();
                }
            });
        });
    }   
 }

 // Singleton
 module.exports = new Server();