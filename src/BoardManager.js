class BoardManager {
    constructor(board, room) {
        this.board = board;
        this.turn = "p1";
        this.yOffset = 10;
        this.xOffset = 10;
        this.room = room;
        this.piecesMovedWithoutEating = 0;
    }

    static createFromCache(roomData, board) {
        let boardManager  = new BoardManager();
        boardManager.board = board;
        boardManager.turn = roomData.currentTurn;
        boardManager.piecesMovedWithoutEating = roomData.piecesMovedWithoutEating;
        return boardManager;
    }

    destroy () {
        this.room = null;
        this.board = null;
    }

    getPiecesMovedWithoutEating() {
        return this.piecesMovedWithoutEating;
    }

    getCellByPosition(y, x) {
        if (this.board[y] === undefined) {
            return false;
        }

        let cell = this.board[y][x];

        if (cell === undefined) {
            return false;
        }

        return cell;
    };

    getOpponentByPlayer(player) {
        if (player === "p1")
            return "p2";
        else if (player === "p2")
            return "p1";
    }

    getBoard() {
        return this.board;
    }

    getTurn() {
        return this.turn;
    }

    /**
     * Move the piece to the new position and also checks the state
     *
     * @returns {boolean} - If movement correct after all check, move piece and return true to broadcast movement to the other client.
     * @param data
     */
    movePiece(data) {

        let piece = data.pieceToMove;
        let destinationCell = data.toCell;

        let fromCell = this.getCellByPosition(piece.y, piece.x);
        let toCell = this.getCellByPosition(destinationCell.y, destinationCell.x);
        let cellEaten = data.cellToEat;
        let pieceToMove = fromCell.getPiece();

        if (this.turn !== piece.player) {
            return false;
        }

        if (pieceToMove.getId() !== piece.id) {
            return false;
        }

        if (data.convertPieceToKing) {
            pieceToMove.convertToKing();
        }

        // Check valid movements
        let pieceToMoveValidMovementsCollection = pieceToMove.getValidMovements(this);
        let isMovementInValidMovements = this.isMovementValid(pieceToMoveValidMovementsCollection, toCell);

        if (isMovementInValidMovements === false) {
            return false;
        }

        this.piecesMovedWithoutEating++;
        if (cellEaten !== undefined) {
            let cellToRemovePiece = this.getCellByPosition(cellEaten.y, cellEaten.x);
            let pieceToRemove = cellToRemovePiece.getPiece();
            if (pieceToRemove.getId() !== cellEaten.piece.id) {
                return false;
            }

            this.piecesMovedWithoutEating = 0;
            cellToRemovePiece.removePiece();
            let player = this.room.getPlayerByTag(pieceToRemove.getPlayer());
            player.removePiece(pieceToRemove);
            let currentPlayer = this.room.getPlayerByTag(pieceToMove.getPlayer());
            currentPlayer.addEatenPiece(pieceToRemove);
        }

        // Updating board
        fromCell.removePiece();
        pieceToMove.updatePosition(toCell.getCellY(), toCell.getCellX());
        toCell.setPiece(pieceToMove);

        return true;
    }

    isMovementValid (validMovements, toCell) {
        validMovements = validMovements.asArray();
        let canMove = false;
        for (let i = 0; i < validMovements.length; i++) {
            let validMovement = validMovements[i];
            let vY = validMovement.getY();
            let vX = validMovement.getX();

            canMove = (vY === toCell.getCellY() && vX === toCell.getCellX());

            if (canMove) {
                return validMovement;
            }
        }

        return canMove;
    };
    
    changeTurn() {
        if (this.turn === "p1") {
            this.turn = "p2";
        } else if (this.turn === "p2") {
            this.turn = "p1";
        }
        
    }
};

module.exports = BoardManager;
