const logger = require('./Service/Logger.js');

class PlayerPool {

    constructor () {
        this.players = new Map();
    }

    addPlayer(player) {
        this.players.set(player.getPlayerId(), player);
    }

    removeFromQueue(player) {
        this.players.delete(player.getPlayerId());
    }

    async matchPlayers() {

        // Initial check to avoid looping, since we need atleast 2 users to being matched up
        if (this.players.size < 2) {
            return [false, false];
        }
        
        logger.info("Started matching users event");
        let playerUsernamesIterator = this.players.entries();
        let playerUsernamesIterator2 = this.players.entries();
        
        let player1 = false;
        let player2 = false;
        
        for (let i = 0; i < this.players.size; i++) {
            let player1Entry = playerUsernamesIterator.next().value;
            let player1BettingRoom = player1Entry[1].userData.bettingRoom;

            for (let z = 0; z < this.players.size; z++) { 
                let player2Entry = playerUsernamesIterator2.next().value;
                if (player2Entry[1].getPlayerId() === player1Entry[1].getPlayerId()) {
                    continue;
                }

                let player2BettingRoom = player2Entry[1].userData.bettingRoom;

                if (player1BettingRoom === player2BettingRoom) {
                    player1 = player1Entry;
                    player2 = player2Entry;
                    break;
                }
            }

            if (player1 !== false && player2 !== false) {
                this.players.delete(player1[0]);
                this.players.delete(player2[0]);
                break;
            }

            // Reset iterator 
            playerUsernamesIterator2 = this.players.entries();
        }

        // No users found
        if (player1 === false || player2 === false) {
            logger.info("No users matched");
            return [false, false];
        }

        logger.info(`Matched player 1: ${player1[1].getPlayerId()} with player 2: ${player2[1].getPlayerId()}`);
        return [player1[1], player2[1]];
   }

};

// Singleton
module.exports = new PlayerPool();
