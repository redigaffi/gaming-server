const log4js = require('log4js');

class Logger {
    constructor() {        
        const logger = log4js.getLogger();
        logger.level = log4js.levels.ALL;
        this.logger = logger;
    }

    getLogger() {
        return this.logger;
    }
}

// Singleton
module.exports = new Logger().getLogger();