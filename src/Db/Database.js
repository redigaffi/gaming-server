const logger = require('../Service/Logger.js');
const mongoose = require('mongoose');

class Database {
  constructor() {
    this._connect()
  }
  
_buildUrl() {
  const env = process.env;
  return `mongodb://${env.MONGO_USERNAME}:${env.MONGO_PASSWORD}@${env.MONGO_HOST}:${env.MONGO_PORT}/${env.MONGO_DATABASE}?authSource=admin`;
}

_connect() {
     mongoose.connect(this._buildUrl(),  { useFindAndModify: false, useNewUrlParser: true })
       .then(() => {
          logger.info('MongoDB connection successful')
       })
       .catch(err => {
          logger.error('MongoDB connection error')
          logger.error(err);
       });
  }
}

// Singleton
module.exports = new Database();