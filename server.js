require('dotenv').config();
require('./src/Db/Database.js');
const logger = require('./src/Service/Logger.js');
const io = require('socket.io')();
io.origins('*:*');
const Server = require('./src/Server.js');

logger.info("Server booting");

// Catch uncaught exceptions
process.on('uncaughtException', (error) => {
    logger.error(error.stack);
    process.exit(1);
});

//Bootup load from cache if available (in case server crashed/updated)
Server.recoverStateFromCache();

setInterval(() => Server.matchPlayersAndCreateRoom(), process.env.MATCH_PLAYERS_INTERVAL);
setInterval(() => Server.clearFinishedMatches(), process.env.CLEAR_FINISHED_MATCHES_INTERVAL);
setInterval(() => Server.retryFailedApiRequests(), process.env.FAILED_API_REQUESTS_RETRY_INTERVAL);

if ("true" === process.env.STORE_STATE) {
    logger.info("Store state in MongoDB");
    setInterval(() => Server.storeState(), process.env.STORE_STATE_INTERVAL);
}

io.on('connection', (socket) => {
    socket.on('start_session',  data => Server.startSessionEvent(data, socket));
    socket.on('disconnect',     ()   => Server.disconnectEvent(socket));
});

io.listen(process.env.SERVER_PORT);
logger.info(`Server listening on port ${process.env.SERVER_PORT}`);
 
 
  
