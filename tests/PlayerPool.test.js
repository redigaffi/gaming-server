const PlayerPool = require('../src/PlayerPool.js');
const PlayerClass = require('../src/Player.js');



test('Test player matched with other player in same betting room', async () => {

    const users = [
        new PlayerClass(12, "", {bettingRoom: 3}),
        new PlayerClass(1, "", {bettingRoom: 1}),
        new PlayerClass(3, "", {bettingRoom: 3}),
        new PlayerClass(4, "", {bettingRoom: 1}),
        new PlayerClass(5, "", {bettingRoom: 2}),
        new PlayerClass(2, "", {bettingRoom: 2}),
        new PlayerClass(7, "", {bettingRoom: 1}),
        new PlayerClass(9, "", {bettingRoom: 3}),
        new PlayerClass(6, "", {bettingRoom: 3}),
        new PlayerClass(8, "", {bettingRoom: 2}),
        new PlayerClass(10, "", {bettingRoom: 1}),
        new PlayerClass(11, "", {bettingRoom: 2}),
        new PlayerClass(13, "", {bettingRoom: 3}),
        new PlayerClass(14, "", {bettingRoom: 1}),
        new PlayerClass(15, "", {bettingRoom: 3}),
        new PlayerClass(16, "", {bettingRoom: 1}),
        new PlayerClass(17, "", {bettingRoom: 2}),
        new PlayerClass(18, "", {bettingRoom: 2}),
        new PlayerClass(19, "", {bettingRoom: 1}),
        new PlayerClass(20, "", {bettingRoom: 3}),
        new PlayerClass(21, "", {bettingRoom: 3}),
        new PlayerClass(22, "", {bettingRoom: 2}),
        new PlayerClass(23, "", {bettingRoom: 1}),
        new PlayerClass(24, "", {bettingRoom: 2}),
        new PlayerClass(25, "", {bettingRoom: 4}),
    ];
    
    users.forEach(player => PlayerPool.addPlayer(player));

    let matchedPlayers = [];

    for(let i = 0; i<12; i++) {
        const users = await PlayerPool.matchPlayers();    

        if (users[0] !== false && users[1] !== false) {
            matchedPlayers.push(users);
        }
    }

    for(let i = 0; i<matchedPlayers.length; i++) { 
        const [player1, player2] = matchedPlayers[i];
        expect(player1.userData.bettingRoom).toEqual(player2.userData.bettingRoom);
        expect(player1.getPlayerId()).not.toEqual(player2.getPlayerId());
    }

    expect(matchedPlayers.length).toEqual(12)
    expect(PlayerPool.players.size).toEqual(1)
    PlayerPool.players.clear();
});

test('Test no player being matched', async () => {
    const users = [
        new PlayerClass(12, "", {bettingRoom: 1}),
        new PlayerClass(1, "", {bettingRoom: 2}),
        new PlayerClass(3, "", {bettingRoom: 3}),
        new PlayerClass(4, "", {bettingRoom: 4}),
        new PlayerClass(5, "", {bettingRoom: 5}),
        new PlayerClass(25, "", {bettingRoom: 6}),
    ];
    
    users.forEach(player => PlayerPool.addPlayer(player));

    for(let i = 0; i<3; i++) {
        const users = await PlayerPool.matchPlayers();
        expect(users).toEqual([false, false]);
    }

    expect(PlayerPool.players.size).toEqual(6);
});